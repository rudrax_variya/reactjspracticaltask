import React, { Fragment, useEffect, useState } from 'react';
import { Card, Button } from 'react-bootstrap'

function Purchased() {
    const [service, setService] = useState([])

    useEffect(()=>{
        fetch("https://api.jsonbin.io/b/5efdf1000bab551d2b6ab1c9/1").then((data)=>{
            data.json().then(result=>{
                console.warn(result)
                setService(result)
            })
        })
    },[])
    return (
        <Fragment>
            <h2>PURCHASED SERVICES</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <Card>
                {
                    service.map((item,index)=>
                    <tr key={index}>
                        {/* <td>{item.}</td>
                        <td>{item.}</td>
                        <td>{item.}</td>
                        <td>{item.}</td> */}
                    </tr>
                    )
                }
            </Card>
        </Fragment>
    )
}

export default Purchased;
